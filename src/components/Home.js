import React, { useReducer } from "react";
import { MuiThemeProvider } from "@material-ui/core/styles";
import theme from "../theme";
import {
  Card,
  CardContent,
  CardHeader,
  Button,
  TextField,
  makeStyles,
} from "@material-ui/core";
import { auth } from "../FireBaseConfig";

// Styling

const useStyles = makeStyles((theme) => ({
  header: {
    textAlign: "center",
    paddingTop: "10vh",
  },
  content: {
    textAlign: "center",
  },
  loginFields: {
    marginLeft: 10,
    marginTop: 10,
  },
  buttons: {
    marginTop: 25,
    fontWeight: "bold",
    fontSize: 12,
  },
  signUp: {
    padding: 15,
  },
  signIn: {
    marginLeft: 15,
    padding: 15,
  },
}));

const Home = (props) => {
  // State constants

  const classes = useStyles();
  const initialState = {
    email: "",
    password: "",
    user: false,
  };
  const reducer = (state, newState) => ({ ...state, ...newState });
  const [state, setState] = useReducer(reducer, initialState);

  // Functions

  const handleChange = (event) => {
    setState({ [event.target.name]: event.target.value });
  };

  /*
   * Registers a user with the application
   */
  const register = () => {
    auth
      .createUserWithEmailAndPassword(state.email, state.password)
      .then((firebaseUser) => {
        props.registerStatus("Registration Successful");
      })
      .catch((error) => {
        props.registerStatus(error.message);
      });
  };

  /*
   * Signs a user into the application
   */
  const login = () => {
    auth
      .signInWithEmailAndPassword(state.email, state.password)
      .then((firebaseUser) => {
        props.loginStatus("Login Successful");
      })
      .catch((error) => {
        props.loginStatus(error.message);
      });
  };

  // Rendering

  return (
    <MuiThemeProvider theme={theme}>
      <Card>
        <CardHeader
          title="Capture your product lifecycles"
          color="inherit"
          className={classes.header}
        />
        {!props.signedIn && (
          <CardContent className={classes.content}>
            <TextField
              id="outlined-email-input"
              label="Email"
              name="email"
              variant="outlined"
              className={classes.loginFields}
              onChange={handleChange}
              value={state.email}
            />
            <TextField
              id="outlined-password-input"
              label="Password"
              type="password"
              name="password"
              autoComplete="current-password"
              variant="outlined"
              className={classes.loginFields}
              onChange={handleChange}
              value={state.password}
            />
            <div className={classes.buttons}>
              <Button
                className={classes.signUp}
                variant="contained"
                color="primary"
                size="large"
                onClick={register}
              >
                Sign Up
              </Button>
              <Button
                className={classes.signIn}
                variant="contained"
                color="primary"
                size="large"
                onClick={login}
              >
                Sign In
              </Button>
            </div>
          </CardContent>
        )}
      </Card>
    </MuiThemeProvider>
  );
};

export default Home;
