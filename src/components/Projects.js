import React, { useReducer, useEffect } from "react";
import { MuiThemeProvider } from "@material-ui/core/styles";
import theme from "../theme";
import {
  Card,
  CardContent,
  CardHeader,
  Button,
  TextField,
  makeStyles,
  List,
  ListItem,
  ListItemText,
  ListItemIcon
} from "@material-ui/core";
import FolderOpenOutlinedIcon from "@material-ui/icons/FolderOpenOutlined";
import { Link } from "react-router-dom";
import { db, auth } from "../FireBaseConfig";
import firebase from "firebase/app";
import "@firebase/firestore";

const useStyles = makeStyles(theme => ({
  card: {
    marginBottom: 10
  },
  header: {
    textAlign: "center",
    paddingTop: "10vh"
  },
  content: {
    textAlign: "center"
  },
  fields: {
    marginLeft: 10,
    marginTop: 10
  },
  buttons: {
    marginTop: 25,
    fontWeight: "bold",
    fontSize: 12
  },
  projectList: {
    width: "80%",
    marginLeft: "10%",
    marginRight: "10%"
  }
}));

const Projects = props => {
  const classes = useStyles();

  const initialState = {
    users: null,
    productName: "",
    teamName: "",
    teamNum: 0,
    startDate: firebase.firestore.Timestamp.fromDate(new Date()),
    velocity: 0,
    hours: 0,
    projects: []
  };
  const reducer = (state, newState) => ({ ...state, ...newState });
  const [state, setState] = useReducer(reducer, initialState);
  const PROJECTINFO = "projectinfo";

  useEffect(() => {
    const unsubscribe = auth.onAuthStateChanged(firebaseUser => {
      if (firebaseUser) {
        setState({ users: [firebaseUser.uid] });
        let userProjects = [];
        db.collection(PROJECTINFO)
          .where("users", "array-contains", firebaseUser.uid)
          .get()
          .then(querySnapshot => {
            querySnapshot.forEach(doc => {
              userProjects.push({ id: doc.id, data: doc.data() });
            });
            setState({ projects: userProjects });
          })
          .catch(error => {
            console.log(`Error getting documents: ${error}`);
          });
      }
    });
    return () => unsubscribe();
  }, []);

  const handleChange = event => {
    setState({ [event.target.name]: event.target.value });
  };

  const handleItemSelection = item => {
    props.item(item);
  };

  /*
   * Creates a new project for the user
   */
  const createProject = () => {
    db.collection(PROJECTINFO)
      .add({
        users: state.users,
        productName: state.productName,
        teamName: state.teamName,
        teamNum: state.teamNum,
        startDate: state.startDate,
        velocity: state.velocity,
        hours: state.hours
      })
      .then(docRef => {
        props.snackMsg(`${state.productName} was created!`);
      })
      .catch(error => {
        props.snackMsg(`Error adding document: ${error}`);
      });
  };

  return (
    <MuiThemeProvider theme={theme}>
      <Card className={classes.card}>
        <CardHeader
          title="Create a new project"
          color="inherit"
          className={classes.header}
        />
        <CardContent className={classes.content}>
          <TextField
            id="velocity"
            label="Initial Velocity"
            name="velocity"
            variant="outlined"
            className={classes.fields}
            onChange={handleChange}
            value={state.velocity}
          />
          <TextField
            id="hours"
            label="Hours per story point"
            name="hours"
            variant="outlined"
            className={classes.fields}
            onChange={handleChange}
            value={state.hours}
          />
          <TextField
            id="productName"
            label="Product Name"
            name="productName"
            variant="outlined"
            className={classes.fields}
            onChange={handleChange}
            value={state.productName}
          />
          <TextField
            id="teamName"
            label="Team Name"
            name="teamName"
            variant="outlined"
            className={classes.fields}
            onChange={handleChange}
            value={state.teamName}
          />
          <TextField
            id="teamNum"
            label="Team Number"
            name="teamNum"
            variant="outlined"
            className={classes.fields}
            onChange={handleChange}
            value={state.teamNum}
          />
          <div className={classes.buttons}>
            <Button
              className={classes.buttons}
              variant="contained"
              color="primary"
              size="large"
              onClick={createProject}
            >
              Create
            </Button>
          </div>
        </CardContent>
      </Card>
      {state.projects.length > 0 && (
        <Card className={classes.card}>
          <CardHeader
            title="Current projects"
            color="inherit"
            className={classes.header}
          />
          <CardContent>
            <List className={classes.projectList}>
              {state.projects.map(item => (
                <ListItem
                  button
                  key={item.id}
                  component={Link}
                  to="/productbacklog"
                  onClick={handleItemSelection.bind(this, item)}
                >
                  <ListItemIcon>
                    <FolderOpenOutlinedIcon />
                  </ListItemIcon>
                  <ListItemText
                    primary={item.data.productName}
                    secondary={item.data.teamName}
                  />
                </ListItem>
              ))}
            </List>
          </CardContent>
        </Card>
      )}
    </MuiThemeProvider>
  );
};
export default Projects;
