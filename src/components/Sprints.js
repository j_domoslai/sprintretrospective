import React, { useEffect, useState } from "react";
import { MuiThemeProvider } from "@material-ui/core/styles";
import theme from "../theme";
import {
  Card,
  CardContent,
  CardHeader,
  Button,
  TextField,
  makeStyles,
  List,
  ListItemText,
  ListItemIcon,
  ListItem,
} from "@material-ui/core";
import { db, auth } from "../FireBaseConfig";
import Autocomplete from "@material-ui/lab/Autocomplete";
import "@firebase/firestore";
import ArrowForwardIosIcon from "@material-ui/icons/ArrowForwardIos";

const useStyles = makeStyles((theme) => ({
  card: {
    marginBottom: 10,
  },
  header: {
    textAlign: "center",
    paddingTop: "10vh",
  },
  content: {
    textAlign: "center",
  },
  fields: {
    marginLeft: 10,
    marginTop: 10,
  },
  buttons: {
    marginTop: 25,
    fontWeight: "bold",
    fontSize: 12,
  },
  projectList: {
    width: "80%",
    marginLeft: "10%",
    marginRight: "10%",
  },
}));

const Sprints = (props) => {
  const classes = useStyles();
  const [stateSprint, setStateSprint] = useState("");
  const [stateStatus, setStateStatus] = useState("");
  const [stateSprintArr, setStateSprintArr] = useState([]);
  const SPRINTS = "sprints";

  useEffect(() => {
    const unsubscribe = auth.onAuthStateChanged((firebaseUser) => {
      if (firebaseUser) {
        db.collection(SPRINTS)
          .where("user", "==", firebaseUser.uid)
          .get()
          .then((querySnapshot) => {
            let sprints = [];
            querySnapshot.forEach((doc) => {
              sprints.push({ id: doc.id, data: doc.data() });
            });
            setStateSprintArr(sprints);
          })
          .catch((error) => {
            console.log(`Error getting documents: ${error}`);
          });
      }
    });
    return () => unsubscribe();
  }, []);

  const createSprint = () => {
    db.collection(SPRINTS)
      .add({
        user: auth.currentUser.uid,
        sprint: stateSprint,
        status: stateStatus,
      })
      .then((docRef) => {
        props.snackMsg(`${stateSprint} was created!`);
      })
      .catch((error) => {
        props.snackMsg(`Error adding document: ${error}`);
      });
  };

  const toggleSprint = (item) => {
    let newStatus = item.data.status === "Open" ? "Closed" : "Open";
    db.collection(SPRINTS)
      .doc(item.id)
      .update({
        status: newStatus,
      })
      .then(() => {
        props.snackMsg(`${item.data.sprint} updated`);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  return (
    <MuiThemeProvider theme={theme}>
      <Card className={classes.card}>
        <CardHeader
          title="Create a new Sprint"
          color="inherit"
          className={classes.header}
        />
        <CardContent className={classes.content}>
          <TextField
            id="sprint"
            label="Sprint Name/Number"
            name="stateSprint"
            variant="outlined"
            className={classes.fields}
            onChange={(e) => setStateSprint(e.target.value)}
            value={stateSprint}
          />
          <Autocomplete
            id="status"
            options={["Open", "Closed"]}
            getOptionLabel={(option) => option}
            onChange={(e, selectedOption) =>
              selectedOption
                ? setStateStatus(`${selectedOption}`)
                : setStateStatus("")
            }
            renderInput={(params) => (
              <TextField
                className={classes.fields}
                style={{ width: "20%" }}
                {...params}
                label="Status"
                variant="outlined"
                fullWidth
              />
            )}
          />
          <div className={classes.buttons}>
            <Button
              className={classes.buttons}
              variant="contained"
              color="primary"
              size="large"
              onClick={createSprint}
            >
              Create
            </Button>
          </div>
        </CardContent>
      </Card>
      {stateSprintArr.length > 0 && (
        <Card>
          <CardHeader
            title="List of Sprints"
            color="inherit"
            className={classes.header}
          />
          <CardContent>
            <List>
              {stateSprintArr.map((item) => (
                <ListItem
                  button
                  key={item.id}
                  onClick={toggleSprint.bind(this, item)}
                >
                  <ListItemIcon>
                    <ArrowForwardIosIcon />
                  </ListItemIcon>
                  <ListItemText
                    primary={item.data.sprint}
                    secondary={item.data.status}
                  />
                </ListItem>
              ))}
            </List>
          </CardContent>
        </Card>
      )}
    </MuiThemeProvider>
  );
};

export default Sprints;
