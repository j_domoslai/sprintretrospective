import React, { useReducer, useEffect, useState } from "react";
import { MuiThemeProvider } from "@material-ui/core/styles";
import theme from "../theme";
import {
  makeStyles,
  Card,
  CardHeader,
  CardContent,
  TextField,
  IconButton,
} from "@material-ui/core";

import { db, auth } from "../FireBaseConfig";
import AddIcon from "@material-ui/icons/Add";
import Autocomplete from "@material-ui/lab/Autocomplete";
import { Link } from "react-router-dom";

const useStyles = makeStyles((theme) => ({
  card: {
    marginBottom: 10,
  },
  header: {
    textAlign: "center",
    paddingTop: "10vh",
  },
  content: {
    textAlign: "center",
  },
  storyField: {
    width: "20%",
    marginLeft: 10,
    marginTop: 10,
  },
  fields: {
    width: "20%",
    marginLeft: 10,
    marginTop: 10,
  },
}));

const UserStory = (props) => {
  const classes = useStyles();

  const initialState = {
    members: [],
    projectName: props.story.data.projectName,
    status: props.story.data.status,
    actualHours: props.story.data.actualHours,
    estimatedHours: props.story.data.estimatedHours,
    userStory: props.story.data.userStory,
    id: "",
    sprint: props.story.data.sprint,
    sprintOpen: true,
    reestimatedHours: "",
  };
  const reducer = (state, newState) => ({ ...state, ...newState });
  const [state, setState] = useReducer(reducer, initialState);
  const TEAMMEMBERS = "teammembers";
  const PRODUCTBACKLOG = "productBacklog";
  const SPRINTS = "sprints";
  const [selection, setSelection] = useState(props.story.data.memberAssigned);
  const handleChange = (event) => {
    setState({ [event.target.name]: event.target.value });
  };
  const onChange = (e, selectedOption) => {
    selectedOption ? setSelection(`${selectedOption}`) : setSelection("");
  };
  useEffect(() => {
    const unsubscribe = auth.onAuthStateChanged((firebaseUser) => {
      if (firebaseUser) {
        setState({ users: [firebaseUser.uid] });
        let members1 = [];
        db.collection(TEAMMEMBERS)
          .get()
          .then((querySnapshot) => {
            querySnapshot.forEach((doc) => {
              if (doc.data().teamName === props.project.data.teamName) {
                members1.push(doc.data().memberName);
              }
            });
            setState({ members: members1 });
          })
          .catch((error) => {
            console.log(`Error getting documents: ${error}`);
          });
        db.collection(PRODUCTBACKLOG)
          .get()
          .then((querySnapshot) => {
            querySnapshot.forEach((doc) => {
              if (
                doc.data().projectName === props.story.data.projectName &&
                doc.data().userStory === props.story.data.userStory
              ) {
                setState({ id: doc.id });
              }
            });
          })
          .catch((error) => {
            console.log(`Error getting documents: ${error}`);
          });
        db.collection(SPRINTS)
          .where("user", "==", firebaseUser.uid)
          .where("sprint", "==", state.sprint)
          .get()
          .then((querySnapshot) => {
            let s = true;
            querySnapshot.forEach((doc) => {
              s = doc.data().status;
            });
            s === "Open"
              ? setState({ sprintOpen: true })
              : setState({ sprintOpen: false });
          })
          .catch((error) => {
            console.log(`Error getting documents: ${error}`);
          });
      }
    });

    return () => unsubscribe();
  }, [
    props.project.data.teamName,
    props.story.data.projectName,
    props.story.data.userStory,
    state.sprint,
  ]);

  const updateStory = () => {
    var ref = db.collection(PRODUCTBACKLOG).doc(state.id);
    return db
      .runTransaction(function (transaction) {
        return transaction.get(ref).then(function (doc) {
          if (!doc.exists) {
            throw new Error("Doc doesnt exist!");
          }
          var changes = {
            actualHours: state.actualHours,
            estimatedHours: state.estimatedHours,
            memberAssigned: selection,
            status: state.status,
            userStory: state.userStory,
            reestimatedHours: state.reestimatedHours,
          };
          transaction.update(ref, changes);
        });
      })
      .then(function () {
        console.log("Update successfully committed!");
      })
      .catch(function (error) {
        console.log("Transaction failed: ", error);
      });
  };
  return (
    <MuiThemeProvider theme={theme}>
      <Card className={classes.card}>
        <CardHeader
          title="Product Backlog"
          subheader={props.project.data.productName}
          color="inherit"
          className={classes.header}
        />
        <CardContent className={classes.content}>
          <TextField
            id="userStory"
            label="User Story"
            name="userStory"
            variant="outlined"
            disabled={!state.sprintOpen}
            className={classes.storyField}
            onChange={handleChange}
            value={state.userStory}
          />

          <TextField
            id="estimatedHours"
            label="Relative Estimate"
            name="estimatedHours"
            disabled={!state.sprintOpen}
            variant="outlined"
            className={classes.fields}
            onChange={handleChange}
            value={state.estimatedHours}
          />

          <TextField
            id="actualHours"
            label="Actual Hours"
            name="actualHours"
            disabled={!state.sprintOpen}
            variant="outlined"
            className={classes.fields}
            onChange={handleChange}
            value={state.actualHours}
          />
          <Autocomplete
            id="status"
            options={["Planned", "Started", "Delivered"]}
            getOptionLabel={(option) => option}
            defaultValue={state.status}
            disabled={!state.sprintOpen}
            onChange={(e, selectedOption) =>
              selectedOption
                ? setState({ status: `${selectedOption}` })
                : setState({ status: "" })
            }
            renderInput={(params) => (
              <TextField
                className={classes.fields}
                style={{ width: "20%" }}
                {...params}
                label="Status"
                variant="outlined"
                fullWidth
              />
            )}
          />
          <Autocomplete
            id="memberName"
            options={state.members}
            getOptionLabel={(option) => option}
            onChange={onChange}
            disabled={!state.sprintOpen}
            defaultValue={selection}
            renderInput={(params) => (
              <TextField
                className={classes.fields}
                style={{ width: "20%" }}
                {...params}
                label="Member Assigned"
                variant="outlined"
                fullWidth
              />
            )}
          />
          {!state.sprintOpen && (
            <TextField
              id="reestimatedHours"
              label="Re-estimated Hours"
              name="reestimatedHours"
              variant="outlined"
              className={classes.fields}
              onChange={handleChange}
              value={state.reestimatedHours}
            />
          )}
          <div>
            <IconButton
              color="primary"
              aria-label="Add story"
              component={Link}
              to="/productbacklog"
              onClick={updateStory}
            >
              <AddIcon />
            </IconButton>
          </div>
        </CardContent>
      </Card>
    </MuiThemeProvider>
  );
};

export default UserStory;
