import React, { useReducer, useEffect, useState } from "react";
import { MuiThemeProvider } from "@material-ui/core/styles";
import theme from "../theme";
import {
  makeStyles,
  Card,
  CardHeader,
  CardContent,
  TextField,
  IconButton,
  List,
  ListItem,
  ListItemText,
  ListItemIcon,
} from "@material-ui/core";

import { db, auth } from "../FireBaseConfig";
import AddIcon from "@material-ui/icons/Add";
import Autocomplete from "@material-ui/lab/Autocomplete";
import { Link } from "react-router-dom";
import ChatBubbleOutline from "@material-ui/icons/ChatBubbleOutline";

const useStyles = makeStyles((theme) => ({
  card: {
    marginBottom: 10,
  },
  header: {
    textAlign: "center",
    paddingTop: "10vh",
  },
  content: {
    textAlign: "center",
  },
  storyField: {
    width: "20%",
    marginLeft: 10,
    marginTop: 10,
  },
  fields: {
    width: "20%",
    marginLeft: 10,
    marginTop: 10,
  },
}));

const ProductBacklog = (props) => {
  const classes = useStyles();

  const initialState = {
    members: [],
    stories: [],
    projectName: "",
    status: "",
    actualHours: 0,
    estimatedHours: 0,
    userStory: "",
    sprints: [],
    sprint: "",
  };
  const reducer = (state, newState) => ({ ...state, ...newState });
  const [state, setState] = useReducer(reducer, initialState);
  const TEAMMEMBERS = "teammembers";
  const PRODUCTBACKLOG = "productBacklog";
  const SPRINTS = "sprints";
  const [selection, setSelection] = useState("");
  const handleChange = (event) => {
    setState({ [event.target.name]: event.target.value });
  };
  const onChange = (e, selectedOption) => {
    selectedOption ? setSelection(`${selectedOption}`) : setSelection("");
  };
  useEffect(() => {
    const unsubscribe = auth.onAuthStateChanged((firebaseUser) => {
      if (firebaseUser) {
        setState({ users: [firebaseUser.uid] });
        let members1 = [];
        db.collection(TEAMMEMBERS)
          .get()
          .then((querySnapshot) => {
            querySnapshot.forEach((doc) => {
              if (doc.data().teamName === props.project.data.teamName) {
                members1.push(doc.data().memberName);
              }
            });
            setState({ members: members1 });
          })
          .catch((error) => {
            console.log(`Error getting documents: ${error}`);
          });
        let stories2 = [];
        db.collection(PRODUCTBACKLOG)
          .get()
          .then((querySnapshot) => {
            querySnapshot.forEach((doc) => {
              if (doc.data().projectName === props.project.data.productName) {
                stories2.push({ id: doc.id, data: doc.data() });
              }
            });
            setState({ stories: stories2 });
          })
          .catch((error) => {
            console.log(`Error getting documents: ${error}`);
          });
        db.collection(SPRINTS)
          .where("user", "==", firebaseUser.uid)
          .where("status", "==", "Open")
          .get()
          .then((querySnapshot) => {
            let sprints = [];
            querySnapshot.forEach((doc) => {
              sprints.push({ id: doc.id, data: doc.data() });
            });
            setState({ sprints: sprints });
          })
          .catch((error) => {
            console.log(`Error getting documents: ${error}`);
          });
      }
    });

    return () => unsubscribe();
  }, [
    state.stories,
    props.project.data.productName,
    props.project.data.teamName,
  ]);

  const handleItemSelection = (item) => {
    props.item(item);
  };

  const createStory = () => {
    db.collection(PRODUCTBACKLOG)
      .add({
        user: state.users[0],
        actualHours: state.actualHours,
        estimatedHours: state.estimatedHours,
        memberAssigned: selection,
        projectName: props.project.data.productName,
        status: state.status,
        userStory: state.userStory,
        sprint: state.sprint,
      })
      .then((docRef) => {
        props.snackMsg(`${state.userStory} was created!`);
      })
      .catch((error) => {
        props.snackMsg(`Error adding document: ${error}`);
      });
  };
  return (
    <MuiThemeProvider theme={theme}>
      <Card className={classes.card}>
        <CardHeader
          title="Product Backlog"
          subheader={props.project.data.productName}
          color="inherit"
          className={classes.header}
        />
        <CardContent className={classes.content}>
          <TextField
            id="userStory"
            label="User Story"
            name="userStory"
            variant="outlined"
            className={classes.storyField}
            onChange={handleChange}
            value={state.userStory}
          />

          <TextField
            id="estimatedHours"
            label="Relative Estimate"
            name="estimatedHours"
            variant="outlined"
            className={classes.fields}
            onChange={handleChange}
            value={state.estimatedHours}
          />

          <TextField
            id="actualHours"
            label="Actual Hours"
            name="actualHours"
            variant="outlined"
            className={classes.fields}
            onChange={handleChange}
            value={state.actualHours}
          />
          <Autocomplete
            id="status"
            options={["Planned", "Started", "Delivered"]}
            getOptionLabel={(option) => option}
            onChange={(e, selectedOption) =>
              selectedOption
                ? setState({ status: `${selectedOption}` })
                : setState({ status: "" })
            }
            renderInput={(params) => (
              <TextField
                className={classes.fields}
                style={{ width: "20%" }}
                {...params}
                label="Status"
                variant="outlined"
                fullWidth
              />
            )}
          />
          <Autocomplete
            id="memberName"
            options={state.members}
            getOptionLabel={(option) => option}
            onChange={onChange}
            renderInput={(params) => (
              <TextField
                className={classes.fields}
                style={{ width: "20%" }}
                {...params}
                label="Member Assigned"
                variant="outlined"
                fullWidth
              />
            )}
          />
          <Autocomplete
            id="sprints"
            options={state.sprints}
            getOptionLabel={(option) => option.data.sprint}
            onChange={(e, selectedOption) =>
              selectedOption
                ? setState({ sprint: `${selectedOption.data.sprint}` })
                : setState({ sprint: "" })
            }
            renderInput={(params) => (
              <TextField
                className={classes.fields}
                style={{ width: "20%" }}
                {...params}
                label="Sprint"
                variant="outlined"
                fullWidth
              />
            )}
          />
          <IconButton
            color="primary"
            aria-label="Add story"
            component="span"
            onClick={createStory}
          >
            <AddIcon />
          </IconButton>
        </CardContent>
      </Card>
      {state.stories.length > 0 && (
        <Card className={classes.card}>
          <CardHeader
            title="Current Stories"
            color="inherit"
            className={classes.header}
          />
          <CardContent>
            <List className={classes.projectList}>
              {state.stories.map((item) => (
                <ListItem
                  button
                  key={item.id}
                  component={Link}
                  to="/story"
                  onClick={handleItemSelection.bind(this, item)}
                >
                  <ListItemIcon>
                    <ChatBubbleOutline />
                  </ListItemIcon>
                  <ListItemText
                    primary={`Member: ${item.data.memberAssigned}, Sprint: ${
                      item.data.sprint
                    }, ${
                      state.sprints.find(
                        (sprint) => sprint.data.sprint === item.data.sprint
                      )
                        ? ""
                        : "SPRINT CLOSED,"
                    } Estimated Hours: ${item.data.estimatedHours}, Actual: ${
                      item.data.actualHours
                    }, Status: ${item.data.status}${
                      item.data.reestimatedHours
                        ? `, Re-estimate Hours: ${item.data.reestimatedHours}`
                        : ""
                    }`}
                    secondary={item.data.userStory}
                  />
                </ListItem>
              ))}
            </List>
          </CardContent>
        </Card>
      )}
    </MuiThemeProvider>
  );
};

export default ProductBacklog;
