import React, { useReducer, useEffect, useState } from "react";
import { MuiThemeProvider } from "@material-ui/core/styles";
import theme from "../theme";
import {
  Card,
  CardContent,
  CardHeader,
  Button,
  TextField,
  makeStyles
} from "@material-ui/core";
import { db, auth } from "../FireBaseConfig";
import Autocomplete from "@material-ui/lab/Autocomplete";
import "@firebase/firestore";

const useStyles = makeStyles(theme => ({
  card: {
    marginBottom: 10
  },
  header: {
    textAlign: "center",
    paddingTop: "10vh"
  },
  content: {
    textAlign: "center"
  },
  fields: {
    marginLeft: 10,
    marginTop: 10
  },
  buttons: {
    marginTop: 25,
    fontWeight: "bold",
    fontSize: 12
  },
  projectList: {
    width: "80%",
    marginLeft: "10%",
    marginRight: "10%"
  }
}));

const TeamMembers = props => {
  const classes = useStyles();

  const initialState = {
    users: null,
    memberName: "",
    projects: []
  };
  const reducer = (state, newState) => ({ ...state, ...newState });
  const [state, setState] = useReducer(reducer, initialState);
  const PROJECTINFO = "projectinfo";
  const TEAMMEMBERS = "teammembers";
  const [selection, setSelection] = useState("");

  const onChange = (e, selectedOption) => {
    selectedOption ? setSelection(`${selectedOption}`) : setSelection("");
  };
  useEffect(() => {
    const unsubscribe = auth.onAuthStateChanged(firebaseUser => {
      if (firebaseUser) {
        setState({ users: [firebaseUser.uid] });
        let userProjects = [];
        db.collection(PROJECTINFO)
          .get()
          .then(querySnapshot => {
            querySnapshot.forEach(doc => {
              userProjects.push(doc.data().teamName);
            });
            setState({ projects: userProjects });
          })
          .catch(error => {
            console.log(`Error getting documents: ${error}`);
          });
      }
    });

    return () => unsubscribe();
  }, []);

  const handleChange = event => {
    setState({ [event.target.name]: event.target.value });
  };

  /*
   * Creates a new project for the user
   */
  const createTeamMember = () => {
    db.collection(TEAMMEMBERS)
      .add({
        memberName: state.memberName,
        teamName: selection
      })
      .then(docRef => {
        props.snackMsg(`${state.memberName} was created!`);
      })
      .catch(error => {
        props.snackMsg(`Error adding document: ${error}`);
      });
  };

  return (
    <MuiThemeProvider theme={theme}>
      <Card className={classes.card}>
        <CardHeader
          title="Create a new project"
          color="inherit"
          className={classes.header}
        />
        <CardContent className={classes.content}>
          <TextField
            id="memberName"
            label="Member Name"
            name="memberName"
            variant="outlined"
            className={classes.fields}
            onChange={handleChange}
            value={state.memberName}
          />
          <Autocomplete
            id=""
            options={state.projects}
            getOptionLabel={option => option}
            style={{ width: 300 }}
            onChange={onChange}
            renderInput={params => (
              <TextField
                {...params}
                label="Team Name"
                variant="outlined"
                fullWidth
              />
            )}
          />
          <div className={classes.buttons}>
            <Button
              className={classes.buttons}
              variant="contained"
              color="primary"
              size="large"
              onClick={createTeamMember}
            >
              Create
            </Button>
          </div>
        </CardContent>
      </Card>
    </MuiThemeProvider>
  );
};
export default TeamMembers;
