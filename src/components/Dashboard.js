import React, { useReducer, useEffect } from "react";
import { MuiThemeProvider } from "@material-ui/core/styles";
import theme from "../theme";
import { auth, db } from "../FireBaseConfig";
import {
  Card,
  CardHeader,
  CardContent,
  makeStyles,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Typography,
} from "@material-ui/core";
import Person from "@material-ui/icons/Person";

const useStyles = makeStyles((theme) => ({
  header: {
    textAlign: "center",
    paddingTop: "10vh",
  },
}));

const Dashboard = () => {
  const classes = useStyles();
  const PRODUCTBACKLOG = "productBacklog";
  const initialState = {
    accuracy: [],
  };
  const reducer = (state, newState) => ({ ...state, ...newState });
  const [state, setState] = useReducer(reducer, initialState);

  useEffect(() => {
    const unsubscribe = auth.onAuthStateChanged((firebaseUser) => {
      if (firebaseUser) {
        db.collection(PRODUCTBACKLOG)
          .where("user", "==", firebaseUser.uid)
          .get()
          .then((querySnapshot) => {
            let userBackLog = [];
            querySnapshot.forEach((doc) => {
              userBackLog.push({ id: doc.id, data: doc.data() });
            });
            estimateAccuracy(userBackLog);
          })
          .catch((error) => {
            console.log(`Error getting documents: ${error}`);
          });
      }
    });
    return () => unsubscribe();
  }, []);

  const estimateAccuracy = (backlog) => {
    let memberAccuracy = [];
    backlog.map((story) => {
      if (story.data.status !== "Planned") {
        let member = story.data.memberAssigned;
        let estimated = story.data.estimatedHours;
        let actual = story.data.actualHours;
        let memberToUpdate = memberAccuracy.findIndex(
          (m) => m.member === member
        );
        if (memberToUpdate >= 0) {
          let estimatedTotal =
            parseInt(memberAccuracy[memberToUpdate].estimated) +
            parseInt(estimated);
          let actualTotal =
            parseInt(memberAccuracy[memberToUpdate].actual) + parseInt(actual);
          memberAccuracy[memberToUpdate] = {
            member: member,
            estimated: estimatedTotal.toString(),
            actual: actualTotal.toString(),
          };
        } else {
          memberAccuracy.push({
            member: member,
            estimated: estimated,
            actual: actual,
          });
        }
      }

      return story;
    });

    let accArr = memberAccuracy.map((m) => {
      let accuracy = (parseInt(m.actual) / parseInt(m.estimated)) * 100;

      //   return {member: m.member, estimated: m.estimated, actual: m.actual, accuracy: accuracy}
      let val = `Total Estimates: ${m.estimated}, Total Actual Hours: ${
        m.actual
      }, Accuracy: ${Math.round(accuracy).toString()}%`;
      return { key: m.member, value: val };
    });

    setState({ accuracy: accArr });
  };

  return (
    <MuiThemeProvider theme={theme}>
      <Card>
        <CardHeader
          title="Dashboard"
          color="inherit"
          className={classes.header}
        />
        <CardContent>
          <Typography variant="h6">
            Estimate Accuracy for team members
          </Typography>
          <List className={classes.projectList}>
            {state.accuracy.map((item) => (
              <ListItem button key={item.key}>
                <ListItemIcon>
                  <Person />
                </ListItemIcon>
                <ListItemText primary={item.key} secondary={item.value} />
              </ListItem>
            ))}
          </List>
        </CardContent>
      </Card>
    </MuiThemeProvider>
  );
};

export default Dashboard;
