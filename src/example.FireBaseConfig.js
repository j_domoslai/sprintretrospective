import firebase from "firebase/app";
import "@firebase/auth";
import "@firebase/database";
import "@firebase/firestore";

// Your web app's Firebase configuration
var firebaseConfig = {
  apiKey: "",
  authDomain: "",
  databaseURL: "",
  projectId: "",
  storageBucket: "",
  messagingSenderId: "",
  appId: ""
};

let app = firebase.initializeApp(firebaseConfig);

export const db = firebase.firestore(app);
export const auth = app.auth();
