import { createMuiTheme } from "@material-ui/core/styles";
export default createMuiTheme({
  typography: {
    useNextVariants: true
  },
  palette: {
    common: { black: "#000", white: "#fff" },
    background: { paper: "#fff", default: "#fafafa" },
    primary: {
      light: "rgba(94, 146, 243, 1)",
      main: "rgba(21, 101, 192, 1)",
      dark: "rgba(0, 60, 143, 1)",
      contrastText: "rgba(255, 255, 255, 1)"
    },
    secondary: {
      light: "rgba(82, 199, 184, 1)",
      main: "rgba(0, 150, 136, 1)",
      dark: "rgba(0, 103, 91, 1)",
      contrastText: "rgba(0, 0, 0, 1)"
    },
    error: {
      light: "rgba(255, 81, 49, 1)",
      main: "rgba(213, 0, 0, 1)",
      dark: "rgba(155, 0, 0, 1)",
      contrastText: "rgba(255, 255, 255, 1)"
    },
    text: {
      primary: "rgba(0, 0, 0, 0.87)",
      secondary: "rgba(0, 0, 0, 0.54)",
      disabled: "rgba(0, 0, 0, 0.38)",
      hint: "rgba(0, 0, 0, 0.38)"
    }
  }
});
