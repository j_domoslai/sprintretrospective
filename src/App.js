import React, { useReducer } from "react";
import { MuiThemeProvider } from "@material-ui/core/styles";
import theme from "./theme";
import {
  AppBar,
  Toolbar,
  IconButton,
  makeStyles,
  Typography,
  fade,
  Drawer,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Menu,
  MenuItem,
  Snackbar,
} from "@material-ui/core";
import { Alert } from "@material-ui/lab";
import MenuIcon from "@material-ui/icons/Menu";
import FolderOpenOutlinedIcon from "@material-ui/icons/FolderOpenOutlined";
import clsx from "clsx";
import { Route, Link, Redirect, Switch } from "react-router-dom";
import AccountCircle from "@material-ui/icons/AccountCircle";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import { auth } from "./FireBaseConfig";
import Home from "./components/Home";
import Projects from "./components/Projects";
import ProductBacklog from "./components/ProductBacklog";
import TeamMembers from "./components/TeamMember";
import UserStory from "./components/UserStory";
import Dashboard from "./components/Dashboard";
import Sprints from "./components/Sprints";

// Styling

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  appBar: {
    transition: theme.transitions.create(["margin", "width"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    width: `calc(100% - ${drawerWidth}px)`,
    marginLeft: drawerWidth,
    transition: theme.transitions.create(["margin", "width"], {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
    [theme.breakpoints.up("sm")]: {
      display: "block",
    },
  },
  loginDetails: {
    position: "relative",
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.75),
    "&:hover": {
      backgroundColor: fade(theme.palette.common.white, 0.85),
    },
    marginLeft: 10,
    width: "100%",
    [theme.breakpoints.up("sm")]: {
      marginLeft: theme.spacing(1),
      width: "auto",
    },
  },
  loginButton: {
    fontWeight: "bold",
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
  },
}));

const App = () => {
  // State constants

  const classes = useStyles();
  const initialState = {
    open: false,
    loggedIn: false,
    anchorEl: null,
    snackBarOpen: false,
    snackBarMsg: "",
    snackBarSeverity: "info",
    projectSelected: null,
    storySelected: null,
    signedIn: false,
  };
  const reducer = (state, newState) => ({ ...state, ...newState });
  const [state, setState] = useReducer(reducer, initialState);
  const openAnchor = Boolean(state.anchorEl);

  // Functions

  const handleDrawerOpen = () => {
    state.open ? setState({ open: false }) : setState({ open: true });
  };

  const handleMenu = (event) => {
    setState({ anchorEl: event.currentTarget });
  };

  const handleClose = () => {
    setState({ anchorEl: null });
  };

  const snackBarClose = () => {
    setState({ snackBarOpen: false });
  };

  const handleSnack = (msg) => {
    setState({
      snackBarMsg: msg,
      snackBarSeverity: "success",
      snackBarOpen: true,
    });
  };

  /*
   * Signs the current user out of the application
   */
  const signOut = () => {
    setState({ anchorEl: null });

    auth
      .signOut()
      .then(() => {
        if (!auth.currentUser) {
          setState({
            loggedIn: false,
            snackBarMsg: "Sign Out Successful",
            snackBarSeverity: "success",
            snackBarOpen: true,
            signedIn: false,
          });
        }
      })
      .catch((error) => {
        setState({
          snackBarMsg: error.message,
          snackBarSeverity: "error",
          snackBarOpen: true,
        });
      });
  };

  /*
   * Checks if the user successfully logged in to the application
   *
   * @param  {type} var   Login authentication message
   */
  const authCheck = (msg) => {
    msg === "Login Successful"
      ? setState({
          loggedIn: true,
          snackBarMsg: msg,
          snackBarSeverity: "success",
          snackBarOpen: true,
          signedIn: true,
        })
      : setState({
          loggedIn: false,
          snackBarMsg: msg,
          snackBarSeverity: "error",
          snackBarOpen: true,
        });
  };

  /*
   * Checks if the user successfully registered with the application
   *
   * @param  {type}  var   Registration authentication message
   */
  const authRegister = (msg) => {
    msg === "Registration Successful"
      ? setState({
          snackBarMsg: msg,
          snackBarSeverity: "success",
          snackBarOpen: true,
        })
      : setState({
          snackBarMsg: msg,
          snackBarSeverity: "error",
          snackBarOpen: true,
        });
  };

  const handleProjectSelection = (project) => {
    setState({ projectSelected: project });
  };

  const handleStorySelection = (story) => {
    setState({ storySelected: story });
  };

  // Rendering

  return (
    <div className={classes.root}>
      <MuiThemeProvider theme={theme}>
        <AppBar
          position="fixed"
          className={clsx(classes.appBar, {
            [classes.appBarShift]: state.open,
          })}
        >
          <Toolbar>
            {state.loggedIn && (
              <IconButton
                edge="start"
                className={classes.menuButton}
                color="inherit"
                aria-label="open drawer"
                onClick={handleDrawerOpen}
              >
                <MenuIcon />
              </IconButton>
            )}
            <Typography className={classes.title} variant="h6" noWrap>
              Sprint Retrospective
            </Typography>
            {state.loggedIn && (
              <div>
                <IconButton
                  aria-label="account of current user"
                  aria-controls="menu-appbar"
                  aria-haspopup="true"
                  onClick={handleMenu}
                  color="inherit"
                >
                  <AccountCircle />
                </IconButton>
                <Menu
                  id="menu-appbar"
                  anchorEl={state.anchorEl}
                  anchorOrigin={{
                    vertical: "top",
                    horizontal: "right",
                  }}
                  keepMounted
                  transformOrigin={{
                    vertical: "top",
                    horizontal: "right",
                  }}
                  open={openAnchor}
                  onClose={handleClose}
                >
                  <MenuItem onClick={signOut}>
                    <ExitToAppIcon />
                    Sign Out
                  </MenuItem>
                </Menu>
              </div>
            )}
          </Toolbar>
        </AppBar>
        <Drawer
          className={classes.drawer}
          variant="persistent"
          anchor="left"
          open={state.open}
          classes={{
            paper: classes.drawerPaper,
          }}
        >
          <List>
            {["Dashboard", "Projects", "TeamMembers", "Sprints"].map(
              (text, index) =>
                index === 0 ? (
                  <ListItem
                    button
                    key={text}
                    component={Link}
                    to="/dashboard"
                    onClick={handleDrawerOpen}
                  >
                    <ListItemIcon>
                      <FolderOpenOutlinedIcon />
                    </ListItemIcon>
                    <ListItemText primary={text} />
                  </ListItem>
                ) : index === 1 ? (
                  <ListItem
                    button
                    key={text}
                    component={Link}
                    to="/projects"
                    onClick={handleDrawerOpen}
                  >
                    <ListItemIcon>
                      <FolderOpenOutlinedIcon />
                    </ListItemIcon>
                    <ListItemText primary={text} />
                  </ListItem>
                ) : index === 2 ? (
                  <ListItem
                    button
                    key={text}
                    component={Link}
                    to="/teammembers"
                    onClick={handleDrawerOpen}
                  >
                    <ListItemIcon>
                      <FolderOpenOutlinedIcon />
                    </ListItemIcon>
                    <ListItemText primary={text} />
                  </ListItem>
                ) : index === 3 ? (
                  <ListItem
                    button
                    key={text}
                    component={Link}
                    to="/sprints"
                    onClick={handleDrawerOpen}
                  >
                    <ListItemIcon>
                      <FolderOpenOutlinedIcon />
                    </ListItemIcon>
                    <ListItemText primary={text} />
                  </ListItem>
                ) : (
                  <ListItem button key={text}>
                    <ListItemIcon>
                      <FolderOpenOutlinedIcon />
                    </ListItemIcon>
                    <ListItemText primary={text} />
                  </ListItem>
                )
            )}
          </List>
        </Drawer>
        <div>
          <Switch>
            <Route exact path="/" render={() => <Redirect to="/home" />} />
            <Route
              path="/home"
              component={() => (
                <Home
                  loginStatus={authCheck}
                  registerStatus={authRegister}
                  signedIn={state.signedIn}
                />
              )}
            />
            {state.loggedIn ? (
              <Route
                path="/projects"
                component={() => (
                  <Projects
                    snackMsg={handleSnack}
                    item={handleProjectSelection}
                  />
                )}
              />
            ) : (
              <Redirect to="/home" />
            )}
            {state.loggedIn ? (
              <Route
                path="/productbacklog"
                component={() => (
                  <ProductBacklog
                    snackMsg={handleSnack}
                    item={handleStorySelection}
                    project={state.projectSelected}
                  />
                )}
              />
            ) : (
              <Redirect to="/home" />
            )}
            {state.loggedIn ? (
              <Route
                path="/sprints"
                component={() => <Sprints snackMsg={handleSnack} />}
              />
            ) : (
              <Redirect to="/home" />
            )}
            {state.loggedIn ? (
              <Route
                path="/dashboard"
                component={() => <Dashboard snackMsg={handleSnack} />}
              />
            ) : (
              <Redirect to="/home" />
            )}
            {state.loggedIn ? (
              <Route
                path="/story"
                component={() => (
                  <UserStory
                    snackMsg={handleSnack}
                    project={state.projectSelected}
                    story={state.storySelected}
                  />
                )}
              />
            ) : (
              <Redirect to="/home" />
            )}
            <Route
              path="/teammembers"
              component={() => <TeamMembers snackMsg={handleSnack} />}
            />
          </Switch>
        </div>
        <Snackbar
          open={state.snackBarOpen}
          anchorOrigin={{ vertical: "bottom", horizontal: "right" }}
          autoHideDuration={3000}
          onClose={snackBarClose}
        >
          <Alert
            onClose={snackBarClose}
            severity={state.snackBarSeverity}
            variant="filled"
          >
            {state.snackBarMsg}
          </Alert>
        </Snackbar>
      </MuiThemeProvider>
    </div>
  );
};

export default App;
